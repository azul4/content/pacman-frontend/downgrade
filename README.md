# downgrade

Bash script for downgrading one or more packages to a version in your cache or the A.L.A.

https://github.com/pbrisbin/downgrade

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/pacman-frontend/downgrade.git
```
